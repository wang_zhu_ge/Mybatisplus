package com.it001.pojo;

import lombok.Data;

/**
 * @author free
 */
@Data
public class SysUser extends BaseEntity {
    private String name;
    private Integer age;
    private String email;
    private String remark;
}
