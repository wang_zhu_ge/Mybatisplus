package com.it001.pojo;

import lombok.Data;

/**
 * @author free
 */
@Data
public class SysSchool extends BaseEntity {
    private String name;
    private String remark;
    private String dept;
}
