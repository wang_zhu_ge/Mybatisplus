package com.it001.config;

import com.it001.config.mybatisplus.injector.CustomizedSqlInjector;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author free
 */
@Configuration
@MapperScan(basePackages = "com.it001.mapper")
public class MybatisPlusConfig {
    @Bean
    public CustomizedSqlInjector customizedSqlInjector() {
        return new CustomizedSqlInjector();
    }
}
