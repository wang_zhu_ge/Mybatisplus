package com.it001.mapper;

import com.it001.config.mybatisplus.mapper.RootMapper;
import com.it001.pojo.SysSchool;

/**
 * @author：gaoliang
 * @date： 2025/1/9  22:17
 */
public interface SysSchoolMapper extends RootMapper<SysSchool> {
}
