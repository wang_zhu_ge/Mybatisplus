package com.it001.mapper;

import com.it001.config.mybatisplus.mapper.RootMapper;
import com.it001.pojo.SysUser;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author free
 * 在对应的Mapper上面继承基本的类 BaseMapper
 */
@Mapper
public interface SysUserMapper extends RootMapper<SysUser> {

}
