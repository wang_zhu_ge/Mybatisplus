package com.it001.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.it001.mapper.SysUserMapper;
import com.it001.pojo.SysUser;
import com.it001.service.SysUserService;
import org.springframework.stereotype.Service;

/**
 * (SysGoods)表服务实现类
 *
 * @author ADX
 * @since 2021-11-01 15:49:15
 */
@Service
public class SysUsersServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

}

