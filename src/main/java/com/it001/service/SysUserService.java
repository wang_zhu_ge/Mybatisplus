package com.it001.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.it001.pojo.SysUser;

/**
 * (SysGoods)表服务接口
 *
 * @author ADX
 * @since 2021-11-01 15:49:14
 */
public interface SysUserService extends IService<SysUser> {

}

