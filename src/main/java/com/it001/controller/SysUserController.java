package com.it001.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.it001.pojo.SysUser;
import com.it001.service.SysUserService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.List;

/**
 * (sysUser)表控制层
 *
 * @author ADX
 * @since 2021-11-01 15:49:11
 */
@RestController
@RequestMapping("/sysUser")
public class SysUserController {
    /**
     * 服务对象
     */
    @Resource
    private SysUserService sysUserService;

    /**
     * 分页查询所有数据
     *
     * @param page    分页对象
     * @param sysUser 查询实体
     * @return 所有数据
     */
    @GetMapping("/list")
    public void selectAll(Page<SysUser> page, SysUser sysUser) {
        this.sysUserService.page(page, new QueryWrapper<>(sysUser));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/getInfo/{id}")
    public void selectOne(@PathVariable Serializable id) {
        this.sysUserService.getById(id);
    }

    /**
     * 新增数据
     *
     * @param sysUser 实体对象
     * @return 新增结果
     */
    @PostMapping("/add")
    public void insert(@RequestBody SysUser sysUser) {
        this.sysUserService.save(sysUser);
    }

    /**
     * 修改数据
     *
     * @param sysUser 实体对象
     * @return 修改结果
     */
    @PutMapping("/update")
    public void update(@RequestBody SysUser sysUser) {
        this.sysUserService.updateById(sysUser);
    }

    /**
     * 删除数据
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @DeleteMapping("/delete/{ids}")
    public void delete(@PathVariable("ids") List<Long> ids) {
        this.sysUserService.removeByIds(ids);
    }
}

