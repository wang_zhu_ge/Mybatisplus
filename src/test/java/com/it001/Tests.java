package com.it001;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.it001.mapper.SysSchoolMapper;
import com.it001.mapper.SysUserMapper;
import com.it001.pojo.SysSchool;
import com.it001.pojo.SysUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
class Tests {

    // 继承了BaseMapper，所有的方法都来自己父类
    // 我们也可以编写自己的扩展方法！
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysSchoolMapper sysSchoolMapper;

    @Test
    void contextLoads() {
        // 参数是一个 Wrapper ，条件构造器，这里我们先不用 null
        // 查询全部用户
        List<SysUser> sysUsers = sysUserMapper.selectList(null);
        sysUsers.forEach(System.out::println);
    }

    // 测试插入
    @Test
    public void testInsert() {
        SysUser sysUser = new SysUser();
        sysUser.setName("微信公众号:javaTest");
        sysUser.setAge(3);
        sysUser.setEmail("21212@qq.com");
        sysUser.setRemark("21212@qq.com");
        sysUser.setId(IdUtil.getSnowflakeNextId());
        int result = sysUserMapper.insert(sysUser); // 帮我们自动生成id
        System.out.println(result); // 受影响的行数
        System.out.println(sysUser); // 发现，id会自动回填
    }

    // 测试更新
    @Test
    public void testUpdate() {
        SysUser sysUser = new SysUser();
        // 通过条件自动拼接动态sql
        sysUser.setId(1875916917307600896L);
        sysUser.setName("关注公众号：1111");
        sysUser.setAge(204);
        sysUser.setRemark("111");
        // 注意：updateById 但是参数是一个 对象！
        int i = sysUserMapper.updateById(sysUser);
        System.out.println(i);
    }


    // 测试查询
    @Test
    public void testSelectById() {
        SysUser sysUser = sysUserMapper.selectById(1875916917307600896L);
        System.out.println(sysUser);
    }

    // 测试批量查询！
    @Test
    public void testSelectByBatchId() {
        List<SysUser> sysUsers = sysUserMapper.selectBatchIds(Arrays.asList(1, 2, 1875916917307600896L));
        sysUsers.forEach(System.out::println);
    }

    // 按条件查询之一使用map操作
    @Test
    public void testSelectByBatchIds() {
        HashMap<String, Object> map = new HashMap<>();
        // 自定义要查询
        map.put("name", "微信公众号:javaTest");
        map.put("age", 3);

        List<SysUser> sysUsers = sysUserMapper.selectByMap(map);
        sysUsers.forEach(System.out::println);
    }

    // 测试分页查询
    @Test
    public void testPage() {
        //  参数一：当前页
        //  参数二：页面大小
        //  使用了分页插件之后，所有的分页操作也变得简单的！
        Page<SysUser> page = new Page<>(2, 5);
        sysUserMapper.selectPage(page, null);

        page.getRecords().forEach(System.out::println);
        System.out.println(page.getTotal());

    }


    // 测试删除
    @Test
    public void testDeleteById() {
        sysUserMapper.deleteById(1875916917307600896L);
    }

    // 通过id批量删除
    @Test
    public void testDeleteBatchId() {
        sysUserMapper.deleteBatchIds(Arrays.asList(1240620674645544961L, 1240620674645544962L));
    }

    // 通过map删除
    @Test
    public void testDeleteMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "Java");
        sysUserMapper.deleteByMap(map);
    }

    @Test
    void test0() {
        // 查询name不为空的用户，并且邮箱不为空的用户，年龄大于等于12
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper
                .isNotNull("name")
                .isNotNull("email")
                .ge("age", 12);
        sysUserMapper.selectList(wrapper).forEach(System.out::println); // 和我们刚才学习的map对比一下
    }

    @Test
    void test2() {
        // 查询名字Test
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.eq("name", "Test");
        SysUser sysUser = sysUserMapper.selectOne(wrapper); // 查询一个数据，出现多个结果使用List 或者 Map
        System.out.println(sysUser);
    }

    @Test
    void test3() {
        // 查询年龄在 20 ~ 30 岁之间的用户
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        wrapper.between("age", 20, 30); // 区间
        sysUserMapper.selectCount(wrapper);// 查询结果数
    }

    // 模糊查询
    @Test
    void test4() {
        // 查询年龄在 20 ~ 30 岁之间的用户
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        // 左和右  t%
        wrapper.notLike("name", "e")
                .likeRight("email", "t");

        List<Map<String, Object>> maps = sysUserMapper.selectMaps(wrapper);
        maps.forEach(System.out::println);
    }

    // 模糊查询
    @Test
    void test5() {

        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        // id 在子查询中查出来
        wrapper.inSql("id", "select id from user where id<3");
        List<Object> objects = sysUserMapper.selectObjs(wrapper);
        objects.forEach(System.out::println);
    }

    //测试六
    @Test
    void test6() {
        QueryWrapper<SysUser> wrapper = new QueryWrapper<>();
        // 通过id进行排序
        wrapper.orderByAsc("id");
        List<SysUser> sysUsers = sysUserMapper.selectList(wrapper);
        sysUsers.forEach(System.out::println);
    }

    @Test
    void test7() {
        long beginTime = System.currentTimeMillis();
        List<SysUser> list = new ArrayList<>();
        for (int i = 0; i < 500; i++) {
            SysUser sysUser = new SysUser();
            sysUser.setId(IdUtil.getSnowflakeNextId());
            sysUser.setName("测试批量插入" + i);
            sysUser.setEmail("Emai");
            sysUser.setAge(i);
            sysUser.setRemark("测试批量插入" + i);
            list.add(sysUser);
        }
        //耗时：428
        sysUserMapper.insertBatch(list);
        long endTime = System.currentTimeMillis();
        System.out.println("耗时：" + (endTime - beginTime));
    }

    @Test
    void test9() {
        long beginTime = System.currentTimeMillis();
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.last("LIMIT 5");  // 通过 .last() 添加 SQL 的限
        List<SysUser> sysUserPage = sysUserMapper.selectList(wrapper);

        List<SysUser> tearDown = new ArrayList<>();
        for (SysUser sysUser : sysUserPage) {
            sysUser.setEmail("333");
            sysUser.setName("333");
            sysUser.setRemark("333");
            sysUser.setAge(333);
            sysUser.setCreateTime(new Date());
            sysUser.setUpdateTime(new Date());
            tearDown.add(sysUser);
        }
        //耗时：428
        sysUserMapper.updateBatch(tearDown);
        long endTime = System.currentTimeMillis();
        System.out.println("耗时：" + (endTime - beginTime));
    }

    @Test
    void test10() {
        long beginTime = System.currentTimeMillis();
        List<SysSchool> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            SysSchool sysUser = new SysSchool();
            sysUser.setId(IdUtil.getSnowflakeNextId());
            sysUser.setName("测试批量插入" + i);
            sysUser.setRemark("测试批量插入" + i);
            list.add(sysUser);
        }
        //耗时：428
        sysSchoolMapper.insertBatch(list);
        long endTime = System.currentTimeMillis();
        System.out.println("耗时：" + (endTime - beginTime));
    }

    @Test
    void test11() {
        long beginTime = System.currentTimeMillis();
        LambdaQueryWrapper<SysSchool> wrapper = new LambdaQueryWrapper<>();// 通过 .last() 添加 SQL 的限
        List<SysSchool> sysUserPage = sysSchoolMapper.selectList(wrapper);

        List<SysSchool> tearDown = new ArrayList<>();
        for (SysSchool sysUser : sysUserPage) {
            sysUser.setName("333");
            sysUser.setRemark("333");
            sysUser.setCreateTime(new Date());
            sysUser.setUpdateTime(new Date());
            tearDown.add(sysUser);
        }
        //耗时：428
        sysSchoolMapper.updateBatch(tearDown);
        long endTime = System.currentTimeMillis();
        System.out.println("耗时：" + (endTime - beginTime));
    }
}
