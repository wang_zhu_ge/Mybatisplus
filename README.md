# mybatis-plus-demo

#### 介绍
SpringbBoot整合MyBatisplus测试例子,包含了基本操作:增删改查,条件查询,乐观锁,逻辑删除等,可以结合EasyCode使用!

数据库采用mysql5.7,
sql文件在resource/sql/mybatisplus.sql